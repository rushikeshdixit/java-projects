1. declare a method variable with access specifier any one and run program and look for results. 
2. create an car interface write 2 implementations and interface reference calls and implementation classes 
   should have interfaces as well as class methods. 
   Access Interface methods using interface reference.
   Access class methods using interface reference. use class reference to acess interface and class methods.

not create object of abstract class
From java 8 we can have default implementation on interface
only one method interface without implementation is called functional interface

interface
establishing interface on all classes

//Can do:
public interface myInterface{
    void tets();
}
public class A implements myInterface{
    void test(){
        print()
    }
    void hello(){}
    psvm(String args[]){
        myInterface mi = new A()
        mi.test() //success
        mi.hello() //error
    }
}

polymorphism
- compile time - method overloading
- run time - method overriding