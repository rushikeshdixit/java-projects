package ass2;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

public class DOMParser {
    public void parseXml() {
        try {
            String path="/Users/Rushi/Documents/Masters/CompilerConstruction/JavaAssignments/src/ass2/note.xml";
            File fXmlFile = new File(path);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);


            doc.getDocumentElement().normalize();

            System.out.println("Root element :" + doc.getDocumentElement().getNodeName().toUpperCase());

            NodeList nList = doc.getElementsByTagName("note");

            System.out.println("----------------------------");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                System.out.println("\nCurrent Element :" + nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

//                    System.out.println("Staff id : " + eElement.getAttribute("id"));
                    System.out.println("To : " + eElement.getElementsByTagName("to").item(0).getTextContent());
                    System.out.println("From : " + eElement.getElementsByTagName("from").item(0).getTextContent());
                    System.out.println("Heading : " + eElement.getElementsByTagName("heading").item(0).getTextContent());
                    System.out.println("Body : " + eElement.getElementsByTagName("body").item(0).getTextContent());

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        DOMParser dom = new DOMParser();
        dom.parseXml();
    }
}
