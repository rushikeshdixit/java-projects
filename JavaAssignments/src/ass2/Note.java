package ass2; /**
 *
 */
import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Rushi
 *
 */
@XmlRootElement
class Note implements Serializable {

    //    @XmlElement
//    String username;
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Note [To=" + to + ", From=" + from + ", heading=" + heading + ", body=" + body + "]";
    }

    @XmlElement
    String to;
    String getTo() {
        return this.to;
    }

    void setTo(String to) {
        this.to = to;
    }

    @XmlElement
    String from;
    String getFrom() {
        return this.from;
    }

    void setFrom(String from) {
        this.from = from;
    }

    @XmlElement
    String heading;
    String getHeading() {
        return this.heading;
    }

    void setHeading(String heading) {
        this.heading = heading;
    }

    @XmlElement
    String body;
    String getBody() {
        return this.body;
    }

    void setBody(String body) {
        this.body = body;
    }

}