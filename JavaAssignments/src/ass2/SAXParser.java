package ass2;

import javax.xml.parsers.SAXParserFactory;
import java.io.File;

public class SAXParser {
    public static void main(String[] args) {

        try {
            String path = "/Users/Rushi/Documents/Masters/CompilerConstruction/JavaAssignments/src/ass2/note.xml";
            File inputFile = new File(path);
            SAXParserFactory factory = SAXParserFactory.newInstance();
            javax.xml.parsers.SAXParser saxParser = factory.newSAXParser();
            CustomSAXParser parserHandler = new CustomSAXParser();
            saxParser.parse(inputFile, parserHandler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
