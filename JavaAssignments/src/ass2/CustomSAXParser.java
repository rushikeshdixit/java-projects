package ass2;


import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class CustomSAXParser extends DefaultHandler {

    private boolean isTo = false;
    private boolean isFrom = false;
    private boolean isHeading = false;
    private boolean isBody = false;

    @Override
    public void startElement(String uri,
                             String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("note")) {
//            String  = attributes.getValue("");
            System.out.println("Encountered start element :"+ qName);
        } else if (qName.equalsIgnoreCase("To")) {
            isTo = true;
        } else if (qName.equalsIgnoreCase("From")) {
            isFrom = true;
        } else if (qName.equalsIgnoreCase("Heading")) {
            isHeading = true;
        } else if (qName.equalsIgnoreCase("Body")) {
            isBody = true;
        }
    }

    @Override
    public void endElement(String uri,
                           String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("note")) {
            System.out.println("End Element :" + qName);
        }
    }

    public void characters (char ch[], int start, int length)
            throws SAXException
    {
        if (isTo) {
            System.out.println("To: "
                    + new String(ch, start, length));
            isTo = false;
        } else if (isFrom) {
            System.out.println("From: " + new String(ch, start, length));
            isFrom = false;
        } else if (isHeading) {
            System.out.println("Heading: " + new String(ch, start, length));
            isHeading = false;
        } else if (isBody) {
            System.out.println("Body: " + new String(ch, start, length));
            isBody = false;
        }
    }

    public void startDocument ()
            throws SAXException
    {
        System.out.println("Encountered start of doc.");
    }

    public void endDocument ()
            throws SAXException
    {
        System.out.println("Encountered end of doc.");
    }
}
