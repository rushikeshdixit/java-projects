package ass2; /**
 *
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 * @author Rushi
 */
public class JAXBXMLParser {

    /**
     *
     */
    public JAXBXMLParser() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
            try {
                String path = "note.xml";
                File file = new File(path);
                JAXBContext jaxbContext = JAXBContext.newInstance(Note.class);

                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                Note note = (Note) jaxbUnmarshaller.unmarshal(file);
                System.out.println(note);
        } catch (JAXBException e) {
            e.printStackTrace();
        }


    }
}
