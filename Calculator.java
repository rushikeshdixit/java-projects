public class Calculator{
    // private String name = "Rushi";
    // public String getName(){
    //     return name;
    // }
    // public void setName(String name){
    //     this.name = name;
    // }
    public int add(int a, int b){
        return a+b;
    }
    public int subtract(int a, int b){
        return a-b;
    }
    public int divide(int a, int b){
        return a/b;
    }
    public int multiply(int a, int b){
        return a*b;
    }
    public static void main(String[] args) {
        Calculator c = new Calculator();
        int m = c.add(1,2);
        int n = c.subtract(4, 2);
        int o = c.divide(6, 3);
        int p = c.multiply(4, 5);
        System.out.println(m);
        System.out.println(n);
        System.out.println(o);
        System.out.println(p);
    }
}