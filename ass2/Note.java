/**
 *
 */
import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Rushi
 *
 */
@XmlRootElement
class User implements Serializable {

    //    @XmlElement
//    String username;
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Note [To=" + username + ", From=" + age + ", heading=" + birthday + ", body=" + mood + "]";
    }

    @XmlElement
    String to;
    @XmlElement
    String from;
    @XmlElement
    String heading;
    @XmlElement
    String body;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }