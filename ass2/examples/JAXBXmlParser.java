/**
 *
 */
package org.vik.java.jaxb;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 * @author vmodugu
 */
public class JAXBXmlParser {

    /**
     *
     */
    public JAXBXmlParser() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        JAXBContext context;
        try {
            context = JAXBContext.newInstance(User.class);
            Marshaller marshaller = context.createMarshaller();
            User user = new User();
            user.setAge("33");
            user.setMood("Bright");
            user.setUsername("vikramm");
            user.setBirthday("2-May-1985");
            // pretty print XML
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(user, System.out);
            File outPut = new File("user.xml");
            try {
                FileOutputStream fos = new FileOutputStream(outPut);
                marshaller.marshal(user, fos);

                File file = new File("user.xml");
                JAXBContext jaxbContext = JAXBContext.newInstance(User.class);

                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                User user1 = (User) jaxbUnmarshaller.unmarshal(file);
                System.out.println(user1);
            } catch (FileNotFoundException e) {

                e.printStackTrace();
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        }


    }

}
