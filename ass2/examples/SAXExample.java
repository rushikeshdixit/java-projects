package org.vik.java.xmlp;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;

public class SAXExample {
    public static void main(String[] args) {

        try {
            String path = "/Users/Rushi/Documents/Masters/CompilerConstruction/JavaAssignments/src/ass2/note.xml"
            File inputFile = new File(path);
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            MySAXParser parserHandler = new MySAXParser();
            saxParser.parse(inputFile, parserHandler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
