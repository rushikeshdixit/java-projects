/**
 * 
 */
package org.vik.java.jaxb;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Rushi
 *
 */
@XmlRootElement
class User implements Serializable {
	
	@XmlElement
    String username;
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "User [username=" + username + ", age=" + age + ", birthday=" + birthday + ", mood=" + mood + "]";
	}

	@XmlElement
    String age;
	@XmlElement
    String birthday;
	@XmlElement
    String mood;

    String getMood() { return this.mood; }
    String getBirthday() { return this.birthday; }
    String getAge() { return this.age; }
    String getUsername() { return this.username; }

    void setMood(String mood) 
    { 
    	this.mood = mood; 
    }
    
    void setBirthday(String birthday) 
    { 
    	this.birthday = birthday; 
    }
    
    void setAge(String age) 
    { 
    	this.age = age; 
    }
    
    void setUsername(String username) 
    { 
    	this.username = username; 
    }
}