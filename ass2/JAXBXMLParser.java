/**
 *
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 * @author vmodugu
 */
public class JAXBXmlParser {

    /**
     *
     */
    public JAXBXmlParser() {
        // TODO Auto-generated constructor stub
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        JAXBContext context;
        try {
            context = JAXBContext.newInstance(Note.class);
            Marshaller marshaller = context.createMarshaller();
            Note note = new Note();
            note.setTo("Sunny");
            note.setFrom("Rushi");
            note.setHeading("Greetings!");
            note.setBody("Hello! How are you.");
            // pretty print XML
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(user, System.out);
            File outPut = new File("note.xml");
            try {
                FileOutputStream fos = new FileOutputStream(outPut);
                marshaller.marshal(note, fos);

                File file = new File("note.xml");
                JAXBContext jaxbContext = JAXBContext.newInstance(Note.class);

                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                Note note1 = (Note) jaxbUnmarshaller.unmarshal(file);
                System.out.println(Note1);
            } catch (FileNotFoundException e) {

                e.printStackTrace();
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        }


    }
}
