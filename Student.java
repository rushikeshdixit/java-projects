public class Student{
    private String name = "Rushi";
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public static void main(String[] args) {
        Student s = new Student();
        Student s1 = new Student();
        String res1 = s.getName();
        s1.setName("adjh");
        String res2 = s1.getName();
        System.out.println(res1);
        System.out.println(res2);
        System.out.println();
        res1 = res2;
        System.out.println(res1);
        System.out.println(res2);
    }
}