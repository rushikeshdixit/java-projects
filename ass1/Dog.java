/*
* When object is created the focus goes from bottom to top but never executes until reached to the top.
* Then executes the default constructor and goes back from top to down to the line where it was called.
* Here d object calls default constructor of Dog class which calls paremeterized constructor of Mammal using super.
* Mammal calls default constructor of parent Animal and as Animal is a top class, prints statement in default constructor.
* Then comes back to Mammal class and executes the parameterized constructor from where it went to Animal Class.
* Then Finally comes back to super and as super task is finished it executes the next line printing Dog Default constructor.
*/
class Animal{
    public Animal(){
        // super();
        System.out.println("Animal default constructor");
    }
    public Animal(String a){
        System.out.println("Parameterized Animal");
    }
}
class Mammal extends Animal{
    public Mammal(){
        super("abc");
        System.out.println("Mammal default constructor");
    }
    public Mammal(String m){
        System.out.println("Parameterized Mammal");
    }
}
class Dog extends Mammal{
    public Dog(){
        super("Rushi");
        System.out.println("Dog default constructor");
    }
    public Dog(String d){
        System.out.println("Parameterized Dog");
    }
    public static void main(String[] args) {
        Dog d = new Dog(); // Calls default value from constructor
    }
}