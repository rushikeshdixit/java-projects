import java.util.Arrays;

public class Ass1{
    private String arr [] = new String[10];
    private int a = 10;
    int i = 0;
    
        public void add(String name){
            if (i<=10){
            arr[i++] = name;
            }
            else{
                System.out.println("Array Full");
            }
        }
        public boolean find(String str) throws NameNotFoundException{
                if (Arrays.asList(arr).contains(str))
                    return true;
                else
                    throw new NameNotFoundException("Name not Found in the array");
        }
    // @Override
    // public String toString() {
    //     return "Hello";
    // }
    public static void main(String args[]){
        Ass1 a1 = new Ass1();
        try{
            a1.add("Rushi");
            a1.add("Sunny");
            System.out.println(a1.find("Rushi"));
            System.out.println(a1.find("Dixit"));
            System.out.println(Arrays.asList(a1.arr));
        }
        catch(NameNotFoundException nnfe){
            System.err.println(nnfe);
        }
    }
}

class NameNotFoundException extends Exception{
    public NameNotFoundException(String msg){
        super(msg);
    }
}