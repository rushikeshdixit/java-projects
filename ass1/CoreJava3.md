### XML Parsing

## JAXB
- converts XML to Java Objects

## DOM
- load into memory

## SAX
- event based
- Use when file size is huge

JAXB | DOM | SAX
---|---|---
converts XML to Java Objects | load into memory | event based

### Databases
## DDL
- Deals with the schema (structure) of database

## DML
- Deals with the data in a Database

What is database Sharding?
