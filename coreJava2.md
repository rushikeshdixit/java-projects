## final keyword:
- no overriding methods
- no inheritance whern declared on classes
- values can be changed in the constructor during object initialization or static context
- using Reflections we can change defaut behaviours of classes

## this keyword:
- 

## static
- static block is executed before the constructor unless constructor is static

## Singleton Pattern: 
- constructors are private
- only one object can be created as static variable

```java
public  class Employee{
    private static Employee emp = new Employee();
    private Employee(){
    }
    psvm(){}
}
```