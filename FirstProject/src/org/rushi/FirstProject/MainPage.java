package org.rushi.FirstProject;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import javax.swing.plaf.basic.BasicOptionPaneUI;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.awt.event.ActionListener; // seems to be missing.



public class MainPage extends JFrame /*implements ActionListener*/{


    private File selectedFile;
    private JPanel panel1;
    private JTable table1;
    private JButton button1;
    private JTextField tf;


    private void createUI() {
        JFrame frame = new JFrame();

        frame.setLayout(new BorderLayout());

//        final JTable table = new JTable(10, 4);

        JPanel btnPnl = new JPanel(new BorderLayout());
        JPanel topBtnPnl = new JPanel(new FlowLayout(FlowLayout.TRAILING));
        JPanel bottombtnPnl = new JPanel(new FlowLayout(FlowLayout.CENTER));

        topBtnPnl.add(new JButton("Choose"));
        bottombtnPnl.add(new JButton(""));
        bottombtnPnl.add(new JButton("Add Selected"));

        btnPnl.add(topBtnPnl, BorderLayout.NORTH);
        btnPnl.add(bottombtnPnl, BorderLayout.CENTER);

        table.getTableHeader().setReorderingAllowed(false);



        frame.add(table.getTableHeader(), BorderLayout.NORTH);
        frame.add(table, BorderLayout.CENTER);
        frame.add(btnPnl, BorderLayout.SOUTH);

        frame.setTitle("JTable Example.");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public String selectFile(){
            JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());

            int returnValue = jfc.showOpenDialog(null);
            // int returnValue = jfc.showSaveDialog(null);

            if (returnValue == JFileChooser.APPROVE_OPTION) {
                this.selectedFile = jfc.getSelectedFile();

            }
            return selectedFile.getAbsolutePath();
    }

//    @Override
//    public void actionPerformed(ActionEvent e) {
//        clicked();
//
//    }

    protected void clicked() {

        String filePath = selectFile();
        File file = new File(filePath);

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            // get the first line
            // get the columns name from the first line
            // set columns name to the jtable model
            String firstLine = br.readLine().trim();
            String[] columnsName = firstLine.split(",");
            System.out.println(columnsName);
            DefaultTableModel model = (DefaultTableModel) this.table1.getModel();
            model.setColumnIdentifiers(columnsName);

            // get lines from txt file
            Object[] tableLines = br.lines().toArray();

            // extratct data from lines
            // set data to jtable model
            for (int i = 0; i < tableLines.length; i++) {
                String line = tableLines[i].toString().trim();
                String[] dataRow = line.split(",");
                model.addRow(dataRow);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
//        MainPage mp = new MainPage();
        JFrame frame = new JFrame("MainPage");
//        frame.setContentPane(new MainPage().button1);
//        frame.setContentPane(new MainPage().table1);
        frame.add()
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        frame.setSize(500, 500);
        frame.pack();
        frame.setVisible(true);
        }
}

//class ButtonListener extends MainPage implements ActionListener{
////    ButtonListener(){
////
////    }
//    @Override
//    public void actionPerformed(ActionEvent e) {
////        this.clicked();
//    }
//}
